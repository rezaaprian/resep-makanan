# Resep Masakan

Practicing Android Apps with latest technology and architecture.

* Kotlin
* MVVM
* Data Binding
* Navigation Component
* Dagger Hilt
* Flow
* Data Store
* Room
* Coil
* Motion Layout
